# declearn v2.4.1

Released: 19/06/2024

This is a subminor release that patches JAX support via backend-only changes.

On the side, tests' resilience to missing optional dependencies was fixed.
